import { useState, useRef } from "react";
import "./styles.css";

const Dictionary = () => {
  const [val, setVal] = useState("");
  const [massage, setMassage] = useState("");
  const ref = useRef(null);
  const ref1 = useRef(null);
  const ref2 = useRef(null);
  const customDictionary = [
    {
      word: "React",
      meaning: "A JavaScript library for building user interfaces.",
    },

    { word: "Component", meaning: "A reusable building block in React." },

    { word: "State", meaning: "An object that stores data for a component." },
  ];
  const [data, setData] = useState(customDictionary);

  function onChange(event) {
    setVal(event.target.value);
    ref.current = event.target.value;

    console.log(ref.current);
  }
  function onSearch(event) {
    if (ref.current == null) {
      setMassage("A reusable building block in React.");
      ref2.current = massage;
    }
    if (ref.current != null) {
      {
        customDictionary.map((itm) => {
          if (itm.word.toLowerCase() == ref.current.toLowerCase()) {
            var r = customDictionary.indexOf(itm);
            ref1.current = r;
          }
        });
        console.log(ref1.current);
        if (ref1.current == null) {
          setMassage("Word not found in the dictionary.");
          ref2.current = massage;
        }
        if (ref1.current != null) {
          setMassage(customDictionary[ref1.current].meaning);
          ref2.current = massage;
        }
      }
    }
  }

  return (
    <div>
      <div>
        <h1>Dictionary App</h1>
        <input
          type="text"
          value={val}
          placeholder="Search for a word..."
          onChange={onChange}
        />
        <button onClick={onSearch}>Search</button>
      </div>
      <p> Definition: {massage}</p>
    </div>
  );
};
export default Dictionary;
